![CI Status](https://github.com/franklai/lyric-get-js/workflows/CI/badge.svg)

## Sites

- https://franks543-lyric-get.vercel.app/
- https://franks543-lyric-get.netlify.app/
- https://franks543-lyric-get.azurewebsites.net/

## Offline version

- https://github.com/franklai/lyric-get-electron/releases

